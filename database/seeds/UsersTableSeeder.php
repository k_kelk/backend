<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
            'email' => 'kaarelkelk@gmail.com',
            'password' => Hash::make('secret'),
            'first_name' => 'Kaarel',
            'last_name' => 'Kelk',
            'profile_img_url' => 'http://wpidiots.com/html/writic/red-writic-template/css/img/demo-images/avatar1.jpg'
        ]);
    }
}
