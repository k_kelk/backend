<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    protected $seeders = [
    	'UsersTableSeeder',
    	'LogsTableSeeder'
    ];

    public function run()
    {
		  	for ($i=0; $i < sizeof($this->seeders); $i++) { 
		  		$this->call($this->seeders[$i]);
		  	}
    }
}
