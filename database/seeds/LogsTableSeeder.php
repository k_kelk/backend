<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class LogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	      DB::table('logs')->insert([
	          'log_folder_path' => '/logs',
	          'machine_name' => 'TLN-01-DEV',
	          'description' => '',
	          'user_id' => 1
	      ]);

	      DB::table('logs')->insert([
	          'log_folder_path' => '/logs',
	          'machine_name' => 'TLN-01-REL',
	          'description' => '',
	          'user_id' => 1
	      ]);

	      DB::table('logs')->insert([
	          'log_folder_path' => '/logs',
	          'machine_name' => 'TLN-01-PROD',
	          'description' => '',
	          'user_id' => 1
	      ]);
    }
}
