<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create Models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\Users::class, function (Faker\Generator $faker) {
    return [
        'email' => $faker->email,
        'password' => $faker->password(6,20),
        'remember_token' => str_random(10)
    ];
});
