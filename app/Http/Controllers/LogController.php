<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Log;
use Auth;

class LogController extends Controller
{
/**
* Create a new controller instance.
*
* @return void
*/
    public function __construct()
    {
        $this->middleware('auth');
    }

/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
    public function index(Request $request)
    {
        $log = Auth::user()->log()->get();
        return response()->json(['status' => 'success','result' => $log]);
    }

/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
    public function store(Request $request)
    {
        $this->validate($request, [
            'log_path' => 'required',
            'machine_name' => 'required'
        ]);

        if(Auth::user()->todo()->Create($request->all())){
            return response()->json(['status' => 'success']);
        }
        else {
            return response()->json(['status' => 'fail']);
        }
    }

/**
* Display the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
    public function show($id)
    {
        $log = Log::where('id', $id)->get();
        return response()->json($log);

    }

/**
* Show the form for editing the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
    public function edit($id)
    {
        $todo = Log::where('id', $id)->get();
        return view('log.editlog', ['logs' => $todo]);
    }

/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  int  $id
* @return \Illuminate\Http\Response
*/
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'log_path' => 'filled',
            'machine_name' => 'filled'
        ]);

        $log = Log::find($id);

        if($log->fill($request->all())->save()) {
            return response()->json(['status' => 'success']);
        }

        return response()->json(['status' => 'failed']);
    }

/**
* Remove the specified resource from storage.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
    public function destroy($id)
    {
        if(Log::destroy($id)) {
            return response()->json(['status' => 'success']);
        }
    }
}

?>