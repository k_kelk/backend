<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\Users;

class UsersController extends Controller

{

    public function __construct()

    {

        //  $this->middleware('auth:api');

    }

    /*
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function me(Request $request) {
        //$this->middleware('auth');

        $user = Users::where('api_key', $request->header('Authorization'))->first();
        if (isset($user)) {
            return response()->json([
                'status' => 'success',
                'data'   => $user
            ]);
        } else {
            return response()->json([
                'status' => 'fail',
                'message' => 'User not found'
            ],401);
        }
    }

    public function authenticate(Request $request) {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $user = Users::where('email', $request->input('email'))->first();

        if (isset($user)) {
            if (Hash::check($request->input('password'), $user->password)) {

                $apikey = base64_encode(str_random(40));

                Users::where('email', $request->input('email'))->update(['api_key' => "$apikey"]);;

                return response()->json([
                    'status' => 'success',
                    'api_key' => $apikey
                ]);
            }
            else {
                return response()->json([
                    'status' => 'fail',
                    'message' => 'User Not Found'
                ],401);
            }
        } else {
            return response()->json([
                'status' => 'fail',
                'message' => 'User not found'
            ],401);
        }



    }

}
?>