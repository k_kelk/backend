<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Contracts\Auth\Authenticatable;

use Illuminate\Auth\Authenticatable as AuthenticableTrait;


class Users extends Model implements Authenticatable

{
    use AuthenticableTrait;

    protected $fillable = ['email','password'];

    protected $hidden = [
        'password'
    ];

    public function logs()

    {

        return $this->hasMany('App\Logs','user_id');

    }

}