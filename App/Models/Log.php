<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model

{
    protected $table = 'log';

    protected $fillable = ['log_path','machine_name','user_id','description'];

}