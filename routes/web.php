<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$router->get('/', function () use ($router) {
    return 'Nothing to see here.... Move along!';
});

$router->post('login/','UsersController@authenticate');

$router->group(['prefix' => 'api/'], function ($router) {
    $router->get('me/', 'UsersController@me');

    $router->post('logs/','LogController@store');
    $router->get('logs/', 'LogController@index');
    $router->get('logs/{id}/', 'LogController@show');
    $router->put('logs/{id}/', 'LogController@update');
    $router->delete('logs/{id}/', 'LogController@destroy');
});